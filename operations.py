import pandas as pd
import requests
import streamlit as st


def load_data():
    df = pd.read_csv("nationwide_rebates.csv")
    df['Discount Amount'] = df['Discount Amount'].apply(format_currency)
    return df

def format_currency(amount):
    # Remove any dollar signs, commas and strip spaces after attempting to convert to float
    amount = amount.replace('$', '').replace(',', '')
    amount = amount.strip()  # strip spaces after replacements
    try:
        float_amount = float(amount)
        return "${:,.2f}".format(float_amount)
    except ValueError:
        # If the amount can't be converted to float, return as is.
        return amount



def get_rebates_by_zip(data, zip_code):
    _, state = get_city_state(zip_code)
    return data[data['State'] == state]

# def get_income_eligible_rebates(df, income_eligibility):
#     if income_eligibility == 'Yes':
#         df = df[df['Income Eligible'] == True]
#     elif income_eligibility == 'No':
#         df = df[df['Income Eligible'] == False]
#     return df

def get_income_eligible_rebates(df, income_eligibility):
    if income_eligibility == 'Yes':
        # Identify unique Rebate Name, Incentive Name combinations that have both income-eligible and non-income-eligible rebates
        mixed_combinations = df[df.duplicated(subset=['Rebate Name', 'Incentive Name'], keep=False)]
        mixed_combinations = mixed_combinations.groupby(['Rebate Name', 'Incentive Name']).apply(lambda x: any(x['Income Eligible']) and not all(x['Income Eligible'])).reset_index(name='mixed')

        # Iterate through the mixed combinations and filter only the income-eligible rebates
        for _, row in mixed_combinations[mixed_combinations['mixed']].iterrows():
            mask = (df['Rebate Name'] == row['Rebate Name']) & (df['Incentive Name'] == row['Incentive Name']) & ~df['Income Eligible']
            df = df[~mask]

    elif income_eligibility == 'No':
        df = df[df['Income Eligible'] == False]

    return df


def get_city_state(zipcode):
    api_key = 'AIzaSyAeRsy5v13tgSCplEy8ZLeQ28Y9leJCFj4'
    url = f"https://maps.googleapis.com/maps/api/geocode/json?address={zipcode}&components=country:US&key={api_key}"
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raises a HTTPError if the status is 4xx, 5xx
    except requests.exceptions.RequestException as e:
        print(f"Request failed: {e}")
        return None

    result = response.json()
    results = result.get('results')
    if results:
        city, state = None, None
        for component in results[0]['address_components']:
            if 'locality' in component['types']:
                city = component['long_name']
            elif 'neighborhood' in component['types'] and city is None:
                city = component['long_name']
            elif 'administrative_area_level_1' in component['types']:
                state = component['long_name']

        if city and state:
            return city, state
        else:
            print("City or state not found.")
            return None, None
    else:
        print("No results found for this zipcode.")
        return None, None


def filter_vehicle_rebates(df, vehicle_type, new_or_used):
    if vehicle_type or new_or_used:
        if vehicle_type == "BEV (battery electric)":
            vehicle_type_pattern = r'\bBEV\b|\bEV\b'
        elif vehicle_type == "PHEV (plug in hybrid)":
            vehicle_type_pattern = r'\bPHEV\b'
        else:
            vehicle_type_pattern = ''

        if new_or_used == "New":
            new_or_used_pattern = r'\bNew\b'
        elif new_or_used == "Used":
            new_or_used_pattern = r'\bUsed\b'
        else:
            new_or_used_pattern = ''

        # Filter the DataFrame based on the conditions
        vehicle_mask = df['Incentive Name'].str.contains(vehicle_type_pattern, case=False, regex=True)
        new_or_used_mask = df['Incentive Name'].str.contains(new_or_used_pattern, case=False, regex=True)

        df = df[vehicle_mask & new_or_used_mask]
    return df
