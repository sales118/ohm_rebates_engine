import pandas as pd

def clean_string(value):
    return float(value.replace("$", "").replace(",", "").strip())

def calculate_savings(rebates, cost):
    total_savings = 0
    savings_details = []
    additional_rebates_list = []  # List to collect non-preferred rebates

    # Group rebates by 'Rebate Name'
    grouped_rebates = rebates.groupby('Rebate Name')

    for name, group in grouped_rebates:
        # Check if there is a preferred rebate in the group
        if group['Preferred'].sum() > 0:
            # Use the preferred rebate for calculation
            preferred_rebate = group[group['Preferred'] == 1]
            # Add non-preferred rebates to additional_rebates_list
            additional_rebates_list.append(group[group['Preferred'] == 0])
        else:
            # If no preferred rebate, use all rebates in the group
            preferred_rebate = group

        for _, row in preferred_rebate.iterrows():
            if row['Type']=='Regional':
                continue
            discount_amount = clean_string(row['Discount Amount'])
            discount_percentage = float(row['Discount Percentage'])
            if row['Percentage or Max']:
                savings = min(discount_percentage/100 * cost, discount_amount)
            else:
                savings = discount_amount
            total_savings += savings
            savings_details.append((row['Rebate Name'], row['Incentive Name'], savings))

    # Concatenate all non-preferred rebates if the list is not empty
    if additional_rebates_list:
        additional_rebates = pd.concat(additional_rebates_list, ignore_index=True)
    else:
        additional_rebates = pd.DataFrame()  # Empty DataFrame

    final_cost = max(cost - total_savings, 0)
    return final_cost, savings_details, additional_rebates



def get_federal_rebates(data):
    federal_rebates = data[data['Type'] == 'Federal']
    car_rebates = federal_rebates[~federal_rebates['Incentive Name'].str.contains('Charger')]
    charger_rebates = federal_rebates[federal_rebates['Incentive Name'].str.contains('Charger')]
    return car_rebates, charger_rebates

def get_utility_rebates(data, utility_company_ids):
    utility_rebates = data[data['Rebate Name'].isin(utility_company_ids)]
    car_rebates = utility_rebates[~utility_rebates['Incentive Name'].str.contains('Charger')]
    charger_rebates = utility_rebates[utility_rebates['Incentive Name'].str.contains('Charger')]
    return car_rebates, charger_rebates

def get_air_quality_rebates(data, air_quality_district_ids):
    air_quality_rebates = data[data['Rebate Name'].isin(air_quality_district_ids)]
    car_rebates = air_quality_rebates[~air_quality_rebates['Incentive Name'].str.contains('Charger')]
    charger_rebates = air_quality_rebates[air_quality_rebates['Incentive Name'].str.contains('Charger')]
    return car_rebates, charger_rebates

def get_regional_rebates(data, zip_code):
    regional_rebates = data[data['Type'].isin(["State", "Regional"])]
    car_rebates = regional_rebates[~regional_rebates['Incentive Name'].str.contains('Charger')]
    charger_rebates = regional_rebates[regional_rebates['Incentive Name'].str.contains('Charger')]
    return car_rebates, charger_rebates
