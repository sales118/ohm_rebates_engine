import streamlit as st
from operations import load_data, get_rebates_by_zip, get_income_eligible_rebates
from operations import get_city_state, filter_vehicle_rebates, format_currency
from rebate_calculations import clean_string, calculate_savings, get_federal_rebates
from rebate_calculations import  get_utility_rebates, get_regional_rebates, get_air_quality_rebates
import pandas as pd

from PIL import Image

image = Image.open('logo.png')


def main():
    st.set_page_config(
    page_title="Ohm Rebates Engine",
    page_icon="logo.png",)

    # Creating columns for the navbar layout
    col1, col2, col3 = st.columns([1, 8, 3])

    # Adding a logo image on the left
    with col1:
        st.image(image, width=50)

    # Adding links in the middle (You can adjust the styling as needed)
    with col3:
        st.write(
            """    
            <style>
            .link {
                color: #6E7074 !important; 
                text-decoration: none !important;
                margin-right: 20px;
            }
            .link:hover {
                color: #1A97BE !important;
            }
            </style>
            <a href="https://ohmp2p.com/" class="link">Home</a>
            <a href="https://ohmp2p.com/quote/1" class="link">Installation</a>
            """, 
            unsafe_allow_html=True
        )



    with st.sidebar:
        with st.expander("🧩 How To Use", True):
            instructions = """
            1. Start by entering your personal details and estimated costs. 
            2. Click 'Search' to find all applicable rebates.
            3. Dive into the details of each rebate to understand exactly how you can save!
            """
            st.markdown(instructions)


    st.write("## EV Rebates and Incentives")



    # Section 1 - Zipcode
    zip_code = st.text_input("Enter your zip code:", "")

    # Section 3 - Additional Questions
    additional_questions = st.container()

    help_income = """
     Income-eligible rebates are available to individuals and households that meet certain income criteria. 
     Eligibility may vary based on household size, annual or monthly income, tax filing status, and other factors.
     Income limits may also differ by state, county, or program. Please refer to the specific guidelines provided
     by each program, or consult with a financial advisor to determine if you qualify for income-eligible rebates."""
    income_eligibility = additional_questions.selectbox('Do you qualify for income eligible rebates?', ['No', 'Yes'], help=help_income)

    # Depending on the income eligibility answer, show more questions
    if income_eligibility == 'Yes':
        in_cols = additional_questions.columns(3)
        household_size = in_cols[0].selectbox('What is your household size?', list(range(1, 13)))
        annual_income = in_cols[1].slider('What is your annual income?', 0, 500000, step=1000, format="$%f")
        filing_status = in_cols[2].selectbox("What is your filing status", ["Single", "Head of Household", "Married Filing Seperately","Married Filing Jointly"])

    # Create a 3-column layout
    cols = additional_questions.columns(3)

    # Place the widgets in the columns
    utility_company_placeholder = cols[0].empty()
    utility_company = None
    state_specific_companies = []

    charger_cost = cols[1].number_input("Installation Cost", min_value=0.00, value=1500.00, step=100.00, format="%f")
    installation_cost = cols[2].number_input("Charger Cost", min_value=0.00, value=500.00, step=100.00, format="%f")

    # Section 2 - Vehicle Questions (Optional)
    vehicle_info = st.expander("Vehicle Information (Optional)")
    vehicle_info.markdown("Please fill in the following details if you would like to see vehicle-related rebates.")

    # Creating columns for the three inputs
    col1, col2, col3 = vehicle_info.columns(3)

    # Vehicle Type input in the first column
    vehicle_type = col1.selectbox("Vehicle Type", ["","BEV (battery electric)", "PHEV (plug in hybrid)"])

    # New or Used input in the second column
    new_or_used = col2.selectbox("New or Used", ["", "New", "Used"])

    # Vehicle Cost input in the third column
    vehicle_cost = col3.number_input("Vehicle Cost", min_value=0.00, value=50000.00,  step=100.00, format="%f")


    data = load_data()

    air_quality_districts = []
    state_specific_companies = []
    if zip_code:
        df = get_rebates_by_zip(data, int(zip_code))
        df = get_income_eligible_rebates(df, income_eligibility)
        state_specific_companies = df[(df['Type'] == 'Utility')]['Rebate Name'].unique()
        air_quality_districts = df[(df['Type'] == 'Air Quality District')]['Rebate Name'].unique()
        
    state_specific_companies = list(state_specific_companies)
    state_specific_companies.insert(0, "Not Applicable") # add an empty option at the beginning

    utility_company = utility_company_placeholder.selectbox('Utility Company', state_specific_companies, help="Please select your utility company from the list of eligible providers available in your region. If you don't see your utility company listed, it may not be eligible for EV related rebates.")

    air_quality_districts = list(air_quality_districts)
    air_quality_districts.insert(0, "Not Applicable") # add an empty option at the beginning
    if air_quality_districts:
        air_quality_district = additional_questions.selectbox('Air Quality District', air_quality_districts, help="Choose the air quality district that corresponds to your location from the list of eligible districts. If you don't see your district listed, it may not be applicable to your specific criteria or region. It also might not have any EV related rebates.")


    if st.button("Search"):
        if zip_code:
            df = get_rebates_by_zip(data, int(zip_code))
            df = get_income_eligible_rebates(df, income_eligibility)

            federal_car_rebates, federal_charger_rebates = get_federal_rebates(data)
            regional_car_rebates, regional_charger_rebates = get_regional_rebates(df, zip_code)

            utility_car_rebates = pd.DataFrame()
            utility_charger_rebates = pd.DataFrame()
            if utility_company:
                utility_company_ids = [utility_company]
                utility_car_rebates, utility_charger_rebates = get_utility_rebates(df, utility_company_ids)

            air_quality_car_rebates = pd.DataFrame()
            air_quality_charger_rebates = pd.DataFrame()
            if air_quality_district:
                air_quality_district_ids = [air_quality_district]
                air_quality_car_rebates, air_quality_charger_rebates = get_air_quality_rebates(df, air_quality_district_ids)

            # Show rebates
            if not federal_car_rebates.empty or not \
                   federal_charger_rebates.empty or not \
                   regional_car_rebates.empty or not regional_charger_rebates.empty or not \
                   utility_car_rebates.empty or not utility_charger_rebates.empty or not \
                   air_quality_car_rebates.empty or not air_quality_charger_rebates.empty:
                
                st.markdown("## Rebates")
                if vehicle_type and new_or_used:
                    # Filter vehicle rebates
                    federal_car_rebates = filter_vehicle_rebates(federal_car_rebates, vehicle_type, new_or_used)
                    regional_car_rebates = filter_vehicle_rebates(regional_car_rebates, vehicle_type, new_or_used)

                    # List to hold all the relevant rebates
                    rebate_list = [federal_car_rebates, regional_car_rebates]

                    if utility_company:
                        utility_car_rebates = filter_vehicle_rebates(utility_car_rebates, vehicle_type, new_or_used)
                        rebate_list.append(utility_car_rebates)

                    if air_quality_district:
                        air_quality_car_rebates = filter_vehicle_rebates(air_quality_car_rebates, vehicle_type, new_or_used)
                        rebate_list.append(air_quality_car_rebates)

                    # Concatenate all the rebates in the list
                    vehicle_rebates = pd.concat(rebate_list, ignore_index=True)

                    if not vehicle_rebates.empty:
                        vehicle_savings, vehicle_savings_details, _ = calculate_savings(vehicle_rebates, vehicle_cost)

                        st.markdown("#### Vehicle Savings Snapshot")
                        st.markdown('<div style="display: flex; justify-content: space-between;"><div style="text-align: left;">Original Vehicle Cost</div><div style="text-align: right;">${}</div></div>'.format(vehicle_cost), unsafe_allow_html=True)
                        for rebate_name, _, saving in vehicle_savings_details:
                            st.markdown('<div style="display: flex; justify-content: space-between;"><div style="text-align: left;">{}</div><div style="text-align: right;">(${})</div></div>'.format(rebate_name, saving), unsafe_allow_html=True)
                        st.markdown('<div style="display: flex; justify-content: space-between;"><div style="text-align: left;">Final Vehicle Cost</div><div style="text-align: right;">${}</div></div>'.format(vehicle_savings), unsafe_allow_html=True)

                # Always display charger rebates
                charger_rebates = pd.concat([federal_charger_rebates, regional_charger_rebates, utility_charger_rebates, air_quality_charger_rebates], ignore_index=True)
                # charger_savings, charger_savings_details = calculate_savings(charger_rebates, charger_cost + installation_cost)
                charger_savings, charger_savings_details, _ = calculate_savings(charger_rebates, charger_cost + installation_cost)


                # Charger savings display 
                st.markdown("#### Charger and Installation Savings Snapshot")
                st.markdown('<div style="display: flex; justify-content: space-between;"><div style="text-align: left;">Original Cost</div><div style="text-align: right;">${}</div></div>'.format(charger_cost + installation_cost), unsafe_allow_html=True)
                for rebate_name, _, saving in charger_savings_details:
                    st.markdown('<div style="display: flex; justify-content: space-between;"><div style="text-align: left;">{}</div><div style="text-align: right;">(${})</div></div>'.format(rebate_name, saving), unsafe_allow_html=True)
                st.markdown('<div style="display: flex; justify-content: space-between;"><div style="text-align: left;">Final Cost</div><div style="text-align: right;">${}</div></div>'.format(charger_savings), unsafe_allow_html=True)

                st.markdown('<hr style="border-top: dashed 1px;"></hr>', unsafe_allow_html=True)

                # Create a expander
                details_expander = st.expander('Show Details')
                # Include content in the expander
                with details_expander:
                    # Now display rebates only if vehicle_type and new_or_used is provided by user
                    if vehicle_type and new_or_used:
                        display_rebates(federal_car_rebates, "Federal Car Rebates")
                        display_rebates(utility_car_rebates, "Utility Car Rebates")
                        display_rebates(air_quality_car_rebates, "Air Quality District Car Rebates")
                        display_rebates(regional_car_rebates, "Regional Car Rebates")

                    # Always display charger rebates
                    display_rebates(federal_charger_rebates, "Federal Charger Rebates")
                    display_rebates(utility_charger_rebates, "Utility Charger Rebates")
                    display_rebates(air_quality_charger_rebates, "Air Quality District Charger Rebates")
                    
                    # Display additional rebates with special header and caption
                    display_rebates(regional_charger_rebates, "Regional Charger Rebates", is_additional_rebate=True)
            else:
                st.error("No rebates found for your inputs.")
        else:
            st.error("Please enter a zip code.")
    


    st.markdown("<br>" * 10, unsafe_allow_html=True)
    st.write("Did we get something wrong? Please provide us with feedback")
    iframe = """<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfwh3uBACwqAk-2gvNRDIvog4nhRv3ZDDMAn5cAQ7Lub3mxEw/viewform?embedded=true" width="100%" height="350" frameborder="0" marginheight="100" marginwidth="0">Loading…</iframe>"""
    st.markdown(iframe, unsafe_allow_html=True)

    st.markdown("""
        <style>
        /* CSS code to hide menu and footer */
        #MainMenu {visibility: hidden;}
        footer {visibility: hidden;}

        /* CSS code for footer */
        .footer {
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: white;
            color: black;
            text-align: center;
            padding: 10px;
            position: absolute;
            height: 1.5rem;   
        }
        
        /* CSS code for links */
        .link {
            color: #6E7074 !important; 
            text-decoration: none !important;
        }
        .link:hover {
            color: #1A97BE !important;
        }
        </style>
        """, unsafe_allow_html=True)

    st.markdown("""
        <div class="footer">
        <p><a class="link" href="https://ohmp2p.com/terms" target="_blank">Terms of Use</a> | <a class="link" href="https://ohmp2p.com/privacy" target="_blank">Privacy Policy</a></p>
        <p>© Ohm 2023, All rights reserved.</p>
        </div>
        """, unsafe_allow_html=True)


def display_rebates(df, title, is_additional_rebate=False):
    st.markdown("""
        <style>
        .rebate-name {
            font-size: 14px;
        }
        .rebate-details {
            font-size: 12px;
        }
        .rebate-link {
            font-size: 12px;
        }
        </style>
        """, unsafe_allow_html=True)

    if not df.empty:
        if is_additional_rebate:
            st.markdown("#### Additional Rebates")
            st.caption("Additional rebates are not applied to your total. These could be based on participation in certain specific programs or might be applicable to wider regions that might be relevant. You might need to adjust your calculations based on the actual rules for applying these rebates.")
        # st.markdown(f"#### {title}")
        for _, row in df.iterrows():
            income_cutoff_text = ""
            # Check if the rebate is an income-eligible rebate and the value is not "-"
            if row['Income Eligible'] and row['Income Cutoff'] != '-':
                income_cutoff_text = f"<p class='rebate-details'><b>Income Cutoff:</b> {row['Income Cutoff']}</p>"

            st.markdown(f"""
                        <p class='rebate-name'><b>{row['Rebate Name']}</b></p>
                        <p class='rebate-details'><b>Discount Amount:</b> {row['Discount Amount']}</p>
                        <p class='rebate-details'><b>Details:</b> {row['Requirements']}</p>
                        {income_cutoff_text}
                        <p class='rebate-link'><b><a href="{row['Rebate_Link']}">Link to Rebate</a></b></p>
                        """, unsafe_allow_html=True)



if __name__ == "__main__":
    main()
