from flask import Flask, request
import openai

app = Flask(__name__)

openai.api_key = 'your-openai-api-key'

@app.route('/generate_summary', methods=['POST'])
def generate_summary():
    data = request.json['data']
    # Use OpenAI's API to generate a summary
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=data,
        max_tokens=100
    )
    summary = response.choices[0].text
    return {'summary': summary}

if __name__ == '__main__':
    app.run(port=5000)
